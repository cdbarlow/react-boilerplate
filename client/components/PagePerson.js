import React from 'react';
import PersonGrid from './PersonGrid';
import { RESERVED_WORDS } from '../reducers/persons';
import { Link } from 'react-router';

const NEW_PREFIX = '---';

export default class PagePerson extends React.Component {

	constructor() {
		super()

		this.state = {changed: false}
	}

	// get row data from PersonGrid
	onChange(rows) {
		this.setState({rows, changed: true})
	}

	// update store then redirect back
	onSave(id, isNew) {
		// get row data
		let record = {}
		this.state.rows.forEach(row => {
			record[row[0]] = row[1]
		})

		// update store then redirect back
		if (isNew) {
			let parent = id.substr(NEW_PREFIX.length);
			this.props.addPerson.call(this, parent, record);
		} else {
			this.props.editPerson.call(this, id, record);
		}
		this.props.history.push('/')
	}

	render() {
		const id = this.props.params.id;
		const isNew = id.startsWith(NEW_PREFIX);
		const person = isNew ? {} : this.props.persons[id];
		const keys = Object.keys(person).filter(key => !RESERVED_WORDS.includes(key))

		return (
			<div className="form" ref="form">
				<p>{JSON.stringify(person)}</p>
				<PersonGrid {...this.props} person={person} keys={keys} onChange={this.onChange.bind(this)} />
				<Link to='/' className="button">&lt;&lt; BACK</Link>
				<input type="submit" value="SAVE" className="button" disabled={!this.state.changed}
					onClick={this.onSave.bind(this, id, isNew)} />
			</div>
		);
	}

}
