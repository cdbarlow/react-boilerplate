import React from 'react';
import { Link } from 'react-router';

export default class Main extends React.Component {

	render() {
		return (
			<div>
				<h2><Link to="/">React</Link></h2>
				{ React.cloneElement(this.props.children, this.props) }
			</div>
		);
	}

}
