import React from 'react';
import Person from './Person';

export default class PageHome extends React.Component {

	render() {
		return (
			<Person {...this.props} id='root' />
		);
	}

}
