import React from 'react';
import { Link } from 'react-router';

export default class Person extends React.Component {

	render() {

		// get person data, child IDs minus deleted children, and build child DOM
		let person = this.props.persons[this.props.id];
		let child_ids = person.children.filter(child_id => !('deleted' in this.props.persons[child_id]))
		let children = child_ids.map(child_id => (
			<Person {...this.props} key={child_id} id={child_id} parent_id={this.props.id} />
		));

		return (
			<div key={person.id} className={'person' + (this.props.parent_id ? ' child' : '')}>
				<div>
					<Link to={`/view/${person.id}`}>{person.name}</Link>
					<button className="button delete" disabled={person.id === 'root'}
						onClick={this.props.removePerson.bind(this, person.id)}>&ndash;</button>
					<Link to={`/view/---${person.id}`} className="button">+</Link>
				</div>
				{children}
			</div>
		);
	}

}
