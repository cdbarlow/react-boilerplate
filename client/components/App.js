import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actionCreators from '../actions';

import Main from './Main';

function mapStateToProps(state) {
	return {
		persons: state.persons
	};
}

export function mapDispatchToProps(dispatch) {
	return bindActionCreators(actionCreators, dispatch);
}
 
var App = connect(mapStateToProps, mapDispatchToProps)(Main);

export default App;
