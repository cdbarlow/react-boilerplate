import React from 'react';

export default class PersonGrid extends React.Component {

	constructor(props) {
		super(props)

		this.state = this.props
		this.onChange = this.onChange.bind(this)
	}

	// set initial grid state
	componentWillMount() {
		let keys = [...this.props.keys, '']
		let rows = keys.map(k => (
			[k, k in this.props.person ? this.props.person[k] : '']
		))
		this.setState({rows, row_count: this.props.keys.length})
		this.render()
	}

	// onChange, add and remove empty rows as necessary and send updates to parent
	onChange(e) {
		// get rows data
		let rows = []
		let $keys = this.refs.rows.getElementsByClassName('key')
		for (let i = 0; i < $keys.length; i++) {
			let key = $keys[i].value
			let val = this.refs.rows.getElementsByClassName('val')[i].value
			if (!key.trim() && !val.trim()) {
				continue
			}
			rows.push([key, val])
		}

		// send updates to parent and render
		this.props.onChange(rows)
		rows.push(['', ''])
		this.setState({rows, row_count: rows.length - 1})
		this.render()
	}

	render() {
		// if new row, include name
		let rows = this.state.rows
		if (!rows || !rows[0][0]) {
			rows = [['name', '']]
		}

		return (
			<div ref="rows">
				{rows.map((row, i) => (
					<div key={i}>
						<input type="text" className="key" defaultValue={row[0]} onChange={this.onChange} disabled={row[0] === 'name'} />
						<input type="text" className="val" defaultValue={row[1]} onChange={this.onChange} />
					</div>
				))}
			</div>
		);
	}

}
