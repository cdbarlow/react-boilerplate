import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { Router, Route, IndexRoute } from 'react-router'
import 'babel-polyfill';

// PAGES
import App from './components/App';
import PagePerson from './components/PagePerson';
import PageHome from './components/PageHome';

// CSS
import css from  './styles/app.styl';

// data store
import store, { history } from './store';

render(
	<Provider store={store}>
		<Router history={history}>
			<Route path="/" component={App}>
				<IndexRoute component={PageHome} />
				<Route path="/view/:id" component={PagePerson}></Route>
			</Route>
		</Router>
	</Provider>,
	document.getElementById('root')
);
