import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import persons from './persons';

// combine all reducers
const rootReducer = combineReducers({
	routing: routerReducer,
	persons
});

export default rootReducer;
