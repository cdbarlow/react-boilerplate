// CONSTANTS
export const DEFAULT_STATE = {
		root: {
			id: 'root',
			name: 'John Smith',
			dob: '1905-06-11',
			children: [1, 2, 3]
		},
		1: {
			id: 1,
			name: 'Matt Smith',
			dob: '2000-02-11',
			children: []
		},
		2: {
			id: 2,
			name: 'Jane Smith Doe',
			dob: '2000-02-11',
			children: [4, 5]
		},
		3: {
			id: 3,
			name: 'Jack Smith',
			dob: '2000-02-11',
			children: []
		},
		4: {
			id: 4,
			name: 'John Doe',
			dob: '2000-02-11',
			children: []
		},
		5: {
			id: 5,
			name: 'Joe Doe',
			dob: '2000-02-11',
			children: []
		}
}
export const RESERVED_WORDS = ['id', 'children', 'deleted'];

// ACTIONS
export default function persons(state = [], action) {
	switch (action.type) {
		
		case 'ADD_PERSON': {
			let id = Math.random()
			let record = {
				id,
				children: [],
				...action.record
			}
			if ('' in record) {
				delete record['']
			}
			state[action.parent].children.push(id)

			return {
				...state,
				[id]: record
			};
		}
		
		case 'EDIT_PERSON': {
			let record = {
				id: action.id,
				children: state[action.id].children,
				...action.record
			}
			if ('' in record) {
				delete record['']
			}
			delete state[action.id]

			return {
				...state,
				[action.id]: record
			};
		}

		case 'REMOVE_PERSON': {
			const delPerson = (i) => {
				if (state[i] && state[i].children.length) {
					state[i].children.forEach(person => {
						delPerson(person)
					});
				}
				state[i].deleted = true
			}
			delPerson(action.id)

			return {
				...state
			};
		}

		default:
			return state;
	}
}
