export function addPerson(parent, record) {
  return {type: 'ADD_PERSON', parent, record};
}

export function editPerson(id, record) {
  return {type: 'EDIT_PERSON', id, record};
}

export function removePerson(id){
  return {type: 'REMOVE_PERSON', id};
}
